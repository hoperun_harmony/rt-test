enum {
	NO_IO_AFFINITY = -2
};

#pragma clang diagnostic ignored "-Wduplicate-decl-specifier"

int resolve_affinity(const char *id, struct bitmask *mask);

